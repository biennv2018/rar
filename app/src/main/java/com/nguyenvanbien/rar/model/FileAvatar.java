package com.nguyenvanbien.rar.model;

import com.nguyenvanbien.rar.R;

public class FileAvatar {
    public FileAvatar() {
    }

    public int getFileAvatar(String filePath){
        return getAvatarFileId(getFileType(filePath));
    }

    private String getFileType(String filePath){
        if(filePath.indexOf(".")==-1){
            return "dir";
        }

        return filePath.substring(filePath.lastIndexOf(".")+1);
    }

    private int getAvatarFileId(String fileType){
        switch (fileType){
            case "dir": return R.drawable.ic_folder;
            case "doc":case "docx": return R.drawable.ic_doc;
            case "xlsx": return R.drawable.ic_xlsx;
            case "pdf": return R.drawable.ic_pdf;
            case "txt": return R.drawable.ic_txt;
            case "rar": return R.drawable.ic_rar;
            case "zip": return R.drawable.ic_zip;
            case "mp3": return R.drawable.ic_mp3;
            case "mp4": return R.drawable.ic_video;
            case "jpg": return R.drawable.ic_jpg;
            case "png": return R.drawable.ic_png;
            default: return R.drawable.ic_no_type; //none type
        }
    }
}
