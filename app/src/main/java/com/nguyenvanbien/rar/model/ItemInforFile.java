package com.nguyenvanbien.rar.model;

import com.nguyenvanbien.rar.ui.MyAplication;

public class ItemInforFile {
    private String mNameFile;
    private String mPathFile;
    private int mAvatar;
    private long mLastEditDate;

    public ItemInforFile(String nameFile, String pathFile, long lastEditDate) {
        mNameFile = nameFile;
        mPathFile = pathFile;
        mLastEditDate = lastEditDate;

        setAvatar();
    }

    /**
     * set avatar for file
     */
    private void setAvatar(){
        mAvatar = MyAplication.FILE_AVATAR.getFileAvatar(mPathFile);
    }

    public String getmNameFile() {
        return mNameFile;
    }

    public String getmPathFile() {
        return mPathFile;
    }

    public int getmAvatar() {
        return mAvatar;
    }

    public long getmLastEditDate() {
        return mLastEditDate;
    }
}
