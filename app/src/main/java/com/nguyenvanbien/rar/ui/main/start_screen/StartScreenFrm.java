package com.nguyenvanbien.rar.ui.main.start_screen;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.gelitenight.waveview.library.WaveView;
import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.base.animations.TranslateAnim;
import com.nguyenvanbien.rar.ui.base.fragments.BaseFragment;
import com.nguyenvanbien.rar.ui.main.about_us.AboutUsFrm;
import com.nguyenvanbien.rar.ui.main.list_file_screen.display_file.ListFileFrm;

import static com.nguyenvanbien.rar.utils.constant.Constants.PACKAGE_NAME;

public class StartScreenFrm extends BaseFragment implements View.OnClickListener{
    private WaveView mWvvStorage;
    private TextView mTvStorage, mTvPercent;
    public static FragmentManager mFragmentManager;
    private StoragePresenter mStoragePre;
    private CustomWaveAnimation mWaveAnimation;

    @Override
    public int setContentView() {
        return R.layout.start_screen;
    }

    @Override
    public void findViewByIds() {
        View parent = getView();
        mWvvStorage = (WaveView) parent.findViewById(R.id.wvv_storage);
        mTvStorage = (TextView) parent.findViewById(R.id.tv_storage);
        mTvPercent = (TextView) parent.findViewById(R.id.tv_per);
    }

    @Override
    public void inits() {
        mFragmentManager = getFragmentManager();
        mStoragePre = new StoragePresenter();
        mWaveAnimation = new CustomWaveAnimation(mWvvStorage);
    }

    @Override
    public void initsComponents() {
        setInforStorage();
    }

    @Override
    public void setEvents() {
        View parent = getView();
        parent.findViewById(R.id.imvbtn_about_us).setOnClickListener(this);
        parent.findViewById(R.id.imvbtn_gift).setOnClickListener(this);
        parent.findViewById(R.id.btn_filter_zip_rar).setOnClickListener(this);
        parent.findViewById(R.id.btn_scan_file).setOnClickListener(this);
        parent.findViewById(R.id.imgbtn_rate_no).setOnClickListener(this);
        parent.findViewById(R.id.imgbtn_rate_ok).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setInforStorage();
        mWaveAnimation.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        mWaveAnimation.cancel();
    }

    /***************************
     * Event onClick
     * @param v view is clicked
     ***************************/
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvbtn_about_us:
                AboutUsFrm aboutUsFrm = new AboutUsFrm();
                replaceFragment(mFragmentManager, aboutUsFrm,
                        TranslateAnim.TRANSLATE_SCREEN, R.id.frlo_container, true, true);
                break;

            case R.id.imvbtn_gift:
                onGift();
                break;

            case R.id.btn_filter_zip_rar:
                startListFileFragment(getString(R.string.filter_screen));
                break;

            case R.id.btn_scan_file:
                startListFileFragment(getString(R.string.scan_screen));
                break;

            case R.id.imgbtn_rate_no:
                showMessage("Cảm ơn bạn đã sử dụng ứng dụng của chúng tôi!");
                break;

            case R.id.imgbtn_rate_ok:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+PACKAGE_NAME));
                getContext().startActivity(intent);
                break;

            default:
                break;
        }
    }

    private void onGift() {
        showMessage("Chức năng chưa phát triển!");
    }

    /**
     * start fragment
     * @param screenName name screen display
     */
    private void startListFileFragment(String screenName){
        ListFileFrm fragment = new ListFileFrm();
        fragment.sendValueToFragment(screenName);
        replaceFragment(mFragmentManager, fragment, TranslateAnim.TRANSLATE_SCREEN,
                R.id.frlo_container, true, true);
    }

    /**********************
     * display size storage
     *********************/
    private void setInforStorage() {
        double total = mStoragePre.getTotalSpaceSrorage();
        double free = mStoragePre.getFreeSpaceStorage();
        mTvStorage.setText(round(free, 2)+"/"+round(total, 2)+" Gb");
        mTvPercent.setText(Integer.toString((int)(free*100/total)));

        mWvvStorage.setBorder(5, getResources().getColor(R.color.orage));
        mWvvStorage.setWaveColor(getResources().getColor(R.color.orage_behind),
                getResources().getColor(R.color.orage));
        mWaveAnimation.setPer((float)(free/total));
        mWvvStorage.setWaterLevelRatio((float)(free/total));
    }

    /*****************************************
     * lay count chu so thap phan sau dau phay
     * @param d
     * @param count
     * @return
     *****************************************/
    private double round(double d, int count){
        int temp = (int)(d*Math.pow(10, count));
        return temp/Math.pow(10, count);
    }

}
