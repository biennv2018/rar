package com.nguyenvanbien.rar.ui.main.list_file_screen.display_file;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.model.ItemInforFile;
import java.text.SimpleDateFormat;


/*****************************************************************************
 * adapter RecyclerView
 ****************************************************************************/
class ListFileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private IListFile mIListFile;
    private IViewIsClicked mIViewIsClicked;
    private IEventListener mIEventListener;
    private SimpleDateFormat mDateFormat;

    public ListFileAdapter(IListFile iListFile, IViewIsClicked iViewIsClicked, IEventListener iEventListener) {
        mIListFile = iListFile;
        mIViewIsClicked = iViewIsClicked;
        mIEventListener = iEventListener;

        mDateFormat = new SimpleDateFormat(" dd/MM/yyyy");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_file, parent, false);
        return new FileHolder(view, mIEventListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //set infor view
        ItemInforFile inforFile = mIListFile.getItem(position);
        FileHolder fileHolder = (FileHolder) holder;
        fileHolder.setInforFile(inforFile);

        //change background if view is clicked
        mIViewIsClicked.changeBackgroundView(fileHolder.itemView);

    }

    @Override
    public int getItemCount() {
        return mIListFile.getCount();
    }

    /***********************
     * viewholder type file
     **********************/
    class FileHolder extends RecyclerView.ViewHolder{
        private ImageView mAvatar;
        private TextView mFileName, mEditDate;

        public FileHolder(View itemView, IEventListener iEventListener) {
            super(itemView);

            findViewByIds(itemView);
            setEvents(itemView, iEventListener);
        }

        private void setEvents(View itemView, IEventListener iEventListener) {
            itemView.setOnClickListener(iEventListener);
            itemView.setOnLongClickListener(iEventListener);
        }

        private void findViewByIds(View itemView) {
            mAvatar = (ImageView) itemView.findViewById(R.id.imv_avatar);
            mFileName = (TextView) itemView.findViewById(R.id.tv_file_name);
            mEditDate = (TextView) itemView.findViewById(R.id.tv_date_edit);
        }

        protected void setInforFile(ItemInforFile inforFile){
            mAvatar.setImageResource(inforFile.getmAvatar());
            mFileName.setText(inforFile.getmNameFile());
            mEditDate.setText(mDateFormat.format(inforFile.getmLastEditDate()));
            itemView.setTag(inforFile.getmPathFile());
        }
    }
}
