package com.nguyenvanbien.rar.ui.main.list_file_screen.compress;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.MyAplication;
import com.nguyenvanbien.rar.ui.base.animations.TranslateAnim;
import com.nguyenvanbien.rar.ui.base.dialogs.BaseDialog;
import com.nguyenvanbien.rar.ui.base.fragments.BaseFragment;
import com.nguyenvanbien.rar.ui.main.list_file_screen.display_file.ListFileFrm;
import com.nguyenvanbien.rar.ui.main.start_screen.StartScreenFrm;
import com.nguyenvanbien.rar.utils.LogData;

public class ProcessDialog extends BaseDialog implements IUpdateInfor, View.OnClickListener {
    private CompressPresenter mCompressPre;
    private TextView mTvPath;
    private ProgressBar mPrgrbPrecess;
    private Button mBtnWatch;

    public ProcessDialog(@NonNull Context context, CompressPresenter compressPresenter) {
        super(context);

        mCompressPre = compressPresenter;
        compressPresenter.setIUpdateInfor(this);
        //set name dialog
        ((TextView) findViewById(R.id.tv_process_name)).setText(mCompressPre.getProcessName());
        updateUI();
    }

    @Override
    public int setContentView() {
        return R.layout.process;
    }

    @Override
    public void findViewByIds() {
        ((TextView)findViewById(R.id.tv_process_name)).setText("Đang nén file");
        mTvPath = (TextView) findViewById(R.id.tv_execute_path);
        mPrgrbPrecess = (ProgressBar) findViewById(R.id.prgrb_process);
        mBtnWatch = (Button) findViewById(R.id.btn_watch);
    }

    @Override
    public void inits() {
    }

    @Override
    public void initsComponents() {
        setVisibleWatchButton(false);
        mPrgrbPrecess.setProgress(0);
        mPrgrbPrecess.setMax(100);
    }

    @Override
    public void setEvents() {
        mBtnWatch.setOnClickListener(this);
        findViewById(R.id.btn_cancle).setOnClickListener(this);
    }

    /************************
     * implement IUpdateInfor
     *
     ************************/
    @Override
    public void setVisibleWatchButton(boolean bool) {
        if(bool){
            mBtnWatch.setVisibility(View.VISIBLE);
        }else {
            mBtnWatch.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateProgress(int per) {
        mPrgrbPrecess.setProgress(per);
    }

    @Override
    public void updatePath(String path) {
        mTvPath.setText(path);
    }

    /***************************
     * implement OnClickListener
     * @param v
     ****************************/
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancle:
                onDestroy();
                break;

            case R.id.btn_watch:
                onWatchResultCompress();

                break;

            default:
                break;
        }
    }

    private void onWatchResultCompress() {
        // get Fragment is displaying
        ListFileFrm fileFrm = (ListFileFrm) StartScreenFrm.mFragmentManager.findFragmentByTag(
                ListFileFrm.class.getName());

        if(fileFrm.getScreenName().equals(getContext().getString(R.string.filter_screen))){
            //if fragment is displaying is filter_file_fragment
            //open fragment scanfile
            final ListFileFrm fragment = new ListFileFrm();
            fragment.sendValueToFragment("Scan file");
            BaseFragment.replaceFragment(StartScreenFrm.mFragmentManager, fragment,
                    TranslateAnim.TRANSLATE_SCREEN, R.id.frlo_container, true, true);
            //fragment.reOpenFragment(StartScreenFrm.mFragmentManager, fragment);

            //open direct contain file
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragment.openDirectory(mCompressPre.getParentDir());
                }
            }, 200);
        }else {
            // if fragment is displaying is scan_file_fragment
            fileFrm.openDirectory(mCompressPre.getParentDir());
        }


        onDestroy();
    }

    private void updateUI(){
        new AsyncTask<Void, String, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                String[] datas = new String[2];
                int per = 0;
                while (!mIsDestroy && per<100){
                    try {
                        datas[0] = mCompressPre.getPathIsExecuting();
                        per = (int) mCompressPre.getPercentExecute();
                        datas[1] = per + "";
                        publishProgress(datas);
                        SystemClock.sleep(200);
                    }catch (NullPointerException e){}

                }

                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                //super.onProgressUpdate(values);

                updatePath(values[0]);
                updateProgress(Integer.parseInt(values[1]));
            }
        }.execute();
    }

    @Override
    public void onDestroy() {
        mCompressPre.onDestroy();
        super.onDestroy();
    }
}
