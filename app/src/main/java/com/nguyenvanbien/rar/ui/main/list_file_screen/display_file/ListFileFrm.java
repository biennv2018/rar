package com.nguyenvanbien.rar.ui.main.list_file_screen.display_file;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.model.ItemInforFile;
import com.nguyenvanbien.rar.ui.base.fragments.BaseFragment;
import com.nguyenvanbien.rar.ui.main.list_file_screen.compress.CompressDialog;
import com.nguyenvanbien.rar.ui.main.list_file_screen.compress.CompressPresenter;
import com.nguyenvanbien.rar.ui.main.list_file_screen.compress.ProcessDialog;
import com.nguyenvanbien.rar.utils.LogData;
import com.nguyenvanbien.rar.utils.constant.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ListFileFrm extends BaseFragment implements View.OnClickListener, IListFile, IViewIsClicked{
    private static final String TAG = ListFileFrm.class.getName();
    private TextView mTvDirPath;
    private List<ItemInforFile> mListFile;
    private ListFileAdapter mListFileAdapter;
    private RecyclerView mRccvListFile;
    private String mScreenName, mDirPath;
    private ListFilePresenter mListFilePresenter;
    private CompressPresenter mCompressPresenter;
    public boolean mSelectMore;
    private List<View> mListViewIsClicked;
    private List<String> mListFileIsClicked;
    //click itemView on recycleView
    private IEventListener mIEventListener = new IEventListener() {
        @Override
        public void onClick(View v) {
            //add view to list view is clicked
            addView(v);
            //change background
            changeBackgroundView(v);

            //handle event click
            final String path = (String) v.getTag();
            final View view = v;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(!mSelectMore){
                        resetBackgroundView(view);

                        if(mScreenName.equals(getString(R.string.scan_screen))) {
                            openDirectory(path);
                        }else {
                            showMessage("Chỉ có thể xem!");
                        }
                    }
                }
            }, 100);
        }

        @Override
        public boolean onLongClick(View v) {
            if(!mSelectMore){
                //delete data of event onClickListener
                clear();
                setSelectMore(true);
            }

            addView(v);
            changeBackgroundView(v);
            return true;
        }
    };

    public void sendValueToFragment(String screenName){
        mScreenName = screenName;
    }

    @Override
    public int setContentView() {
        return R.layout.list_file_screen;
    }

    @Override
    public void findViewByIds() {
        View parent = getView();
        mRccvListFile = (RecyclerView) parent.findViewById(R.id.rccv_list_data);
        mTvDirPath = (TextView) parent.findViewById(R.id.tv_dir_path);
    }

    @Override
    public void inits() {
        mSelectMore = false;
        mListFilePresenter = new ListFilePresenter();
        mListFileIsClicked = new ArrayList<>();

        mDirPath = "/";
        openDirectory(mDirPath);

        mCompressPresenter = new CompressPresenter(getContext());
    }

    /**
     * set infor for screen display
     * set screen name
     * set visible file path
     */
    private void setScreenIsDiplayed() {
        View parent = getView();
        ((TextView) parent.findViewById(R.id.tv_screen_name)).setText(mScreenName);
        if(mScreenName.equals(getString(R.string.filter_screen))){
            parent.findViewById(R.id.lnlo_show_path).setVisibility(View.GONE);
        }else {
            mTvDirPath.setText(mDirPath);
        }
    }

    @Override
    public void initsComponents() {
        setScreenIsDiplayed();

        mListFileAdapter = new ListFileAdapter(this, this, mIEventListener);
        mRccvListFile.setAdapter(mListFileAdapter);
        mRccvListFile.setLayoutManager(new LinearLayoutManager(getContext()));

        mListViewIsClicked = new ArrayList<>();
    }

    @Override
    public void setEvents() {
        View parent = getView();

        parent.findViewById(R.id.imvbtn_back).setOnClickListener(this);
        parent.findViewById(R.id.imvbtn_backward).setOnClickListener(this);
        parent.findViewById(R.id.btn_compress).setOnClickListener(this);
        parent.findViewById(R.id.btn_uncompress).setOnClickListener(this);
        parent.findViewById(R.id.btn_detail).setOnClickListener(this);
        parent.findViewById(R.id.btn_delete).setOnClickListener(this);
    }

    public String getScreenName() {
        return mScreenName;
    }

    /*****************
     * event onClick
     * @param v
     ****************/
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imvbtn_back:
                getActivity().onBackPressed();
                break;

            case R.id.imvbtn_backward:
                openDirectory(mDirPath.substring(0, mDirPath.lastIndexOf(File.separator)));
                break;

            case R.id.btn_compress:
                compressFile();
                break;

            case R.id.btn_uncompress:
                unCompressFile();
                break;

            case R.id.btn_detail:
                getDetail();
                break;

            case R.id.btn_delete:
                deleteFile();
                break;

            default:
                break;
        }
    }

    private void deleteFile() {
        if(mListFileIsClicked.size()<1){
            showMessage("Vui lòng chọn file");
            return;
        }
        mCompressPresenter.deleteFiles(mListFileIsClicked);
        SystemClock.sleep(300);
        //delete data
        setSelectMore(false);
        clear();
        reOpendDirrectory();
    }

    private void getDetail() {
        if(mListFileIsClicked.size()!=1){
            showMessage("Vui lòng chọn một file");
        }else {
            LogData.showDialogMessage(getContext(), "Chi tiết",
                    mCompressPresenter.detailFile(mListFileIsClicked.get(0)) );
        }
        setSelectMore(false);
        clear();
    }

    private void unCompressFile() {
        if(mListFileIsClicked.size()!=1){
            showMessage("Vui lòng chọn một file");
        }else {
            String path = mListFileIsClicked.get(0);
            if(!path.endsWith(".zip") && !path.endsWith(".rar")){
                showMessage("Vui lòng chọn file zip/rar");
                return;
            }

            mCompressPresenter.uncompress(path);
            //delete data
            setSelectMore(false);
            clear();

            ProcessDialog processDialog = new ProcessDialog(getContext(), mCompressPresenter);
            processDialog.show();
        }
    }

    private void compressFile() {
        if(mListFileIsClicked.size()<1){
            showMessage("Vui lòng chọn file");
            return;
        }
        CompressDialog compressDialog = new CompressDialog(getContext(),
                mCompressPresenter, this);
        compressDialog.show();
    }


    /*******************************
     * implement interface IListFile
     *******************************/
    @Override
    public int getCount() {
        if(mListFile==null){
            return 0;
        }

        return mListFile.size();
    }

    @Override
    public ItemInforFile getItem(int position) {
        if(mListFile==null){
            return null;
        }
        return mListFile.get(position);
    }


    /*************************************
     * implement interface IViewIsClicked
     ************************************/
    /**
     * add view is clicked to list
     * @param view
     */
    @Override
    public void addView(View view) {
        LogData.log(TAG, "addView", "----------------------------\nadd view to list");
        if(!mSelectMore){
            clear();
            mListViewIsClicked.add(view);
            mListFileIsClicked.add((String)view.getTag());
            LogData.log(TAG, "addView", "Success");
        }else {
            if(!checkExistView(view)){
                mListViewIsClicked.add(view);
                mListFileIsClicked.add((String)view.getTag());
                LogData.log(TAG, "addView", "Success");
            }else {
                removeView(view);
                LogData.log(TAG, "addView", "fail");
            }
        }
    }

    @Override
    public void setSelectMore(boolean bool) {
        mSelectMore = bool;
    }

    @Override
    public boolean checkExistView(View view) {
        if(mListFileIsClicked.indexOf(view.getTag())!=-1){
            return true;
        }

        return false;
    }

    @Override
    public void removeView(View view) {
        mListViewIsClicked.remove(view);
        mListFileIsClicked.remove(view.getTag());
        resetBackgroundView(view);
    }

    @Override
    public void clear() {
        if(mListViewIsClicked!=null&&mListViewIsClicked.size()>0) {
            //reset background
            for(View view : mListViewIsClicked){
                resetBackgroundView(view);
            }

            mListViewIsClicked.clear();
            mListFileIsClicked.clear();
        }
    }


    /***********************************
     * change background view is clicked
     * @param view is clicked
     ***********************************/
    public void changeBackgroundView(final View view) {
        int color;
        if(checkExistView(view)){
            color = Constants.COLOR_VIEW_IS_CLICKED;
        }else {
            color = Constants.DEFAULT_COLOR_VIEW;
        }
        view.setBackgroundResource(color);
    }

    @Override
    public void resetBackgroundView(View view) {
        view.setBackgroundResource(Constants.DEFAULT_COLOR_VIEW);
    }


    /*********************************************************
     * get list file
     * @param dirPath is null if get all copress file
     *                is not null if get all file in directory
     ********************************************************/
    private List<ItemInforFile> getListFile(String dirPath) {
        if(mScreenName.equals(getString(R.string.filter_screen))){
            return mListFilePresenter.getAllCompreesFile();
        }else {
            return mListFilePresenter.getFilesInDir(dirPath);
        }
    }


    /***********************************
     * open directory
     * @param dirPath path of directory
     **********************************/
    public void openDirectory(String dirPath) {
        setSelectMore(false);
        //delete all view is clicked
        clear();

        if(dirPath==null || dirPath.equals("")){
            showMessage("Không thể truy cập!");
            return;
        }

        if(new File(dirPath).isFile()){
            showMessage("Chỉ có thể xem!");
            return;
        }

        List<ItemInforFile> listFiles = getListFile(dirPath);
        if(listFiles==null ){
            showMessage("Không thể mở file!");
        }else {
            //reset all data
            mTvDirPath.setText(dirPath);
            mDirPath = dirPath;
            mListFile = listFiles;

            try {
                mListFileAdapter.notifyDataSetChanged();
            }catch (NullPointerException e){}
        }
    }

    private void reOpendDirrectory(){
        openDirectory(mDirPath);
    }

    /**************************
     * get list file is clicked
     * @return
     **************************/
    @Override
    public List<String> getListFileIsClicked() {
        return mListFileIsClicked;
    }

    @Override
    public void onDestroy() {
        mCompressPresenter.onDestroy();
        super.onDestroy();
    }
}
