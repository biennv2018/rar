package com.nguyenvanbien.rar.ui.main.flash_screen;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

/****************************************************************************
 * xin cap quyen ung dung
 ***************************************************************************/
public class RequestPermisstion {
    private Activity mActivity;
    private boolean mIsRequestingPermisstion;
    private String[] mPermisstion;

    public RequestPermisstion(Activity activity) {
        mActivity = activity;
        mIsRequestingPermisstion = false;
        mPermisstion = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        checkpermisstion(100);

    }

    private void checkpermisstion(int requestCode) {
        List<String> listPermisstion = new ArrayList<>();
        for(String per : mPermisstion) {
            int checkPermisstion = ActivityCompat.checkSelfPermission(mActivity.getBaseContext(), per);

            if(checkPermisstion==PackageManager.PERMISSION_DENIED){
                listPermisstion.add(per);
            }
        }

        if(listPermisstion.size()>0) {
            String[] temp = new String[]{};
            ActivityCompat.requestPermissions(mActivity, listPermisstion.toArray(temp), requestCode);
            mIsRequestingPermisstion = true;
        }
    }

    public boolean isRequestingPermisstion() {
        return mIsRequestingPermisstion;
    }
}
