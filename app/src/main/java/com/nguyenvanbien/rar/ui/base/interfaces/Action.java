package com.nguyenvanbien.rar.ui.base.interfaces;

public interface Action<E> {
    void call(E e);
}
