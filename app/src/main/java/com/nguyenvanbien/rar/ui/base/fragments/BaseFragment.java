package com.nguyenvanbien.rar.ui.base.fragments;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.base.animations.TranslateAnim;
import com.nguyenvanbien.rar.ui.base.interfaces.IBaseMethod;

public abstract class BaseFragment extends Fragment implements IBaseMethod{
    protected boolean mIsDestroy;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(setContentView(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mIsDestroy = false;

        findViewByIds();
        inits();
        initsComponents();
        setEvents();
    }

    @Override
    public void showMessage(String content) {
        Toast toast = Toast.makeText(getContext(), content, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    /**
     * replace fragment
     * @param manager is a RragmentManager
     * @param fragment fragment replace
     * @param translateAnim animation translate view
     * @param container view container fragment
     * @param addToBackStack true add fragment to backStack
     *                       false no add fragment to backStack
     * @param commit true commit transaction
     *               false no commit transaction
     */
    public static <E extends BaseFragment> void replaceFragment(FragmentManager manager, E fragment,
                                       TranslateAnim translateAnim, @IdRes int container,
                                       boolean addToBackStack, boolean commit){
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(
                translateAnim.getEnterToRight(),
                translateAnim.getExitToRight(),
                translateAnim.getEnterToLeft(),
                translateAnim.getExitToLeft()
                );
        String tag = fragment.getClass().getName();
        transaction.replace(container, fragment, tag);

        if(addToBackStack){
            transaction.addToBackStack(tag);
        }

        if(commit){
            transaction.commit();
        }
    }

    public<E extends BaseFragment> void reOpenFragment(FragmentManager manager, E fragment){
        if(manager.findFragmentByTag(fragment.getClass().getName())==null){
            replaceFragment(manager, fragment, TranslateAnim.TRANSLATE_SCREEN, R.id.frlo_container,
                    true, true);
        }

    }

    @Override
    public void onDestroy() {
        mIsDestroy = true;
        super.onDestroy();
    }
}
