package com.nguyenvanbien.rar.ui.main.list_file_screen.display_file;

import android.view.View;

import java.util.List;

public interface IViewIsClicked {
    void addView(View view);

    /**
     * set select more file
     * @param bool
     */
    void setSelectMore(boolean bool);

    boolean checkExistView(View view);

    /***********************************
     * clear all view is clicked in list
     **********************************/
    void clear();

    void removeView(View view);

    /***********************************
     * change background view is clicked
     * @param view is clicked
     **********************************/
    void changeBackgroundView(View view);

    /***************************
     * reset background view
     * @param view
     *************************/
    void resetBackgroundView(View view);

    /***************************
     * get list file is clicked
     * @return
     ***************************/
    List<String> getListFileIsClicked();
}
