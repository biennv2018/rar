package com.nguyenvanbien.rar.ui.main.start_screen;

import com.nguyenvanbien.rar.interact.ManagerFileInStorage;
import com.nguyenvanbien.rar.ui.base.presenters.BasePresenter;

public class StoragePresenter extends BasePresenter{
    private ManagerFileInStorage mMngFile;

    public StoragePresenter() {
        super();

        mMngFile = new ManagerFileInStorage();
    }

    public double getFreeSpaceStorage(){
        return mMngFile.getFreeSpaceStorage();
    }

    public double getTotalSpaceSrorage(){
        return mMngFile.getTotalSpaceStorage();
    }
}
