package com.nguyenvanbien.rar.ui.main.list_file_screen.compress;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.base.dialogs.BaseDialog;
import com.nguyenvanbien.rar.ui.main.list_file_screen.display_file.IViewIsClicked;

/***************************************************************************************
 * Dialog compress file
 **************************************************************************************/
public class CompressDialog extends BaseDialog implements View.OnClickListener {
    private RadioGroup mRdoGrFileType;
    private EditText mEdtFileName;
    private IViewIsClicked mIViewIsClicked;
    private CompressPresenter mCompressPresenter;

    public CompressDialog(@NonNull Context context, CompressPresenter compressPresenter ,IViewIsClicked iViewIsClicked) {
        super(context);
        mIViewIsClicked = iViewIsClicked;
        mCompressPresenter = compressPresenter;
    }

    @Override
    public int setContentView() {
        return R.layout.compress;
    }

    @Override
    public void findViewByIds() {
        mRdoGrFileType = (RadioGroup) findViewById(R.id.rdogr_file_type);
        mEdtFileName = (EditText) findViewById(R.id.edt_file_name);
    }

    @Override
    public void inits() {
    }

    @Override
    public void initsComponents() {
    }

    @Override
    public void setEvents() {
        findViewById(R.id.btn_cancle).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancle:
                onDestroy();
                break;

            case R.id.btn_ok:
                compressFile();
                break;

            default:
                break;
        }
    }

    private void compressFile(){
        String name = mEdtFileName.getText().toString();
        if(name.equals("")){
            showMessage("Tên không thể để trống");
            return;
        }

        if(mIViewIsClicked.getListFileIsClicked().size()<1){
            showMessage("Vui lòng chọn file");
            return;
        }

        int id = mRdoGrFileType.getCheckedRadioButtonId();
        String fileType = "";
        switch (id){
            case R.id.rdobtn_zip:
                fileType = "zip";
                if(name.lastIndexOf(".zip")==-1){
                    name = name + ".zip";
                }
                break;

            case R.id.rdobtn_rar:
                fileType = "rar";
                if(name.lastIndexOf(".rar")==-1){
                    name = name + ".rar";
                }
                break;

            default:
                break;
        }

        mCompressPresenter.compress(fileType, name,
                mIViewIsClicked.getListFileIsClicked());

        ProcessDialog processDialog = new ProcessDialog(getContext(), mCompressPresenter);
        processDialog.show();

        //delete data
        mIViewIsClicked.setSelectMore(false);
        mIViewIsClicked.clear();
        onDestroy();
    }
}
