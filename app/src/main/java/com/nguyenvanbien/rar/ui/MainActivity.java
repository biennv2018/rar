package com.nguyenvanbien.rar.ui;

import android.support.v4.app.FragmentManager;

import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.base.activities.BaseActivity;
import com.nguyenvanbien.rar.ui.base.animations.TranslateAnim;
import com.nguyenvanbien.rar.ui.base.fragments.BaseFragment;
import com.nguyenvanbien.rar.ui.main.list_file_screen.display_file.ListFileFrm;
import com.nguyenvanbien.rar.ui.main.start_screen.StartScreenFrm;

public class MainActivity extends BaseActivity{
    private FragmentManager mManager;

    @Override
    public int setContentView() {
        return R.layout.main_screen;
    }

    @Override
    public void findViewByIds() {

    }

    @Override
    public void inits() {
        mManager = getSupportFragmentManager();
        StartScreenFrm startScreenFrm = new StartScreenFrm();
        BaseFragment.replaceFragment(mManager, startScreenFrm,
                TranslateAnim.TRANSLATE_SCREEN,
                R.id.frlo_container, false, true);
    }

    @Override
    public void initsComponents() {
    }

    @Override
    public void setEvents() {

    }

    @Override
    public void onBackPressed() {
        ListFileFrm fileFrm = (ListFileFrm) mManager.findFragmentByTag(ListFileFrm.class.getName());
        if(fileFrm!=null && fileFrm.mSelectMore){
            //scancle select more
            fileFrm.mSelectMore = false;
            //delete data
            fileFrm.clear();
            return;
        }

        super.onBackPressed();

    }
}
