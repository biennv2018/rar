package com.nguyenvanbien.rar.ui.base.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.Toast;

import com.nguyenvanbien.rar.ui.base.interfaces.IBaseMethod;

public abstract class BaseActivity extends AppCompatActivity implements IBaseMethod{
    protected boolean mIsDestroy;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsDestroy = false;

        setContentView(setContentView());
        findViewByIds();
        inits();
        initsComponents();
        setEvents();
    }

    @Override
    public void showMessage(String content) {
        Toast toast = Toast.makeText(getBaseContext(), content, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    @Override
    public void onDestroy() {
        mIsDestroy = true;
        super.onDestroy();
    }
}
