package com.nguyenvanbien.rar.ui.main.list_file_screen.display_file;

import com.nguyenvanbien.rar.interact.ManagerFileInStorage;
import com.nguyenvanbien.rar.model.ItemInforFile;
import com.nguyenvanbien.rar.ui.base.presenters.BasePresenter;

import java.util.List;

class ListFilePresenter extends BasePresenter {
    private ManagerFileInStorage mMngFile;

    public ListFilePresenter() {
        super();
        mMngFile = new ManagerFileInStorage();
    }

    /**********************
     * get all zip/rar file
     * @return
     **********************/
    List<ItemInforFile> getAllCompreesFile(){
        return mMngFile.getAllCompressFile();
    }

    /****************************
     * get all files in directory
     * @param dirPath
     * @return
     *****************************/
    List<ItemInforFile> getFilesInDir(String dirPath){
        return mMngFile.getFilesInDir(dirPath);
    }
}
