package com.nguyenvanbien.rar.ui.base.presenters;

import com.nguyenvanbien.rar.ui.base.interfaces.Action;
import com.nguyenvanbien.rar.ui.base.interfaces.IBasePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public abstract class BasePresenter implements IBasePresenter {
    protected boolean mIsDestroy;
    private List<Disposable> mDisposableList;

    public BasePresenter() {
        mDisposableList = new ArrayList<>();
        mIsDestroy = false;
    }

    public <E> void interact(Observable<E> ob, final Action<E> onNext, final Action<Throwable> onError){
        ob = ob.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        checkDispose();

        Disposable disposable = ob.subscribe(new Consumer<E>() {
            @Override
            public void accept(E e) throws Exception {
                onNext.call(e);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                onError.call(throwable);
            }
        });

        mDisposableList.add(disposable);
    }

    private void checkDispose(){
        for(Disposable disposable : mDisposableList){
            if(disposable.isDisposed()){
                mDisposableList.remove(disposable);
            }
        }
    }

    @Override
    public void onDestroy() {
        mIsDestroy = true;
        for(Disposable disposable : mDisposableList){
            disposable.dispose();
        }
    }
}
