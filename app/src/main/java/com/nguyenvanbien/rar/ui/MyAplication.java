package com.nguyenvanbien.rar.ui;

import android.app.Application;
import android.content.Context;
import com.nguyenvanbien.rar.model.FileAvatar;

public class MyAplication extends Application{
    public static FileAvatar FILE_AVATAR;
    public static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();

        APP_CONTEXT = getBaseContext();
        FILE_AVATAR = new FileAvatar();
    }
}
