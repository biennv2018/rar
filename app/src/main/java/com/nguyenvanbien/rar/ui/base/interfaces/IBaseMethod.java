package com.nguyenvanbien.rar.ui.base.interfaces;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;

public interface IBaseMethod {
    @LayoutRes int setContentView();
    void findViewByIds();
    void inits();
    void initsComponents();
    void setEvents();
    void showMessage(String content);
    void onDestroy();
}
