package com.nguyenvanbien.rar.ui.main.about_us;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.base.fragments.BaseFragment;
import com.nguyenvanbien.rar.utils.LogData;

import static com.nguyenvanbien.rar.utils.constant.Constants.PACKAGE_NAME;

public class AboutUsFrm extends BaseFragment implements View.OnClickListener {

    @Override
    public int setContentView() {
        return R.layout.about_us;
    }

    @Override
    public void findViewByIds() {

    }

    @Override
    public void inits() {

    }

    @Override
    public void initsComponents() {

    }

    @Override
    public void setEvents() {
        View parent = getView();

        parent.findViewById(R.id.btn_share).setOnClickListener(this);
        parent.findViewById(R.id.btn_rate).setOnClickListener(this);
        parent.findViewById(R.id.btn_infor).setOnClickListener(this);
        parent.findViewById(R.id.imvbtn_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_share:
                onShareApp();
                break;

            case R.id.btn_rate:
                onRateApp();
                break;

            case R.id.btn_infor:
                onInfor();
                break;

            default:
                getActivity().onBackPressed();
                break;
        }
    }

    private void onShareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "App rất hay: https://play.google.com/store/apps/details?id=" + PACKAGE_NAME);
        sendIntent.setType("text/plain");
        getContext().startActivity(sendIntent);
    }

    private void onRateApp(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+PACKAGE_NAME));
        getContext().startActivity(intent);
    }

    private void onInfor(){
        String message = "Tên app: RarRip"
                +"\nChức năng chính: nén/giải nén file rip/rar"
                +"\nTeam phát triển: hitstudio";
        LogData.showDialogMessage(getContext(), "Giới thiệu", message);
    }
}
