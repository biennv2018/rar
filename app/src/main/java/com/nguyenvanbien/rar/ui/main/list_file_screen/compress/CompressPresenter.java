package com.nguyenvanbien.rar.ui.main.list_file_screen.compress;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.view.Gravity;
import android.widget.Toast;

import com.nguyenvanbien.rar.compress_uncompress.CompressUncompressMng;
import com.nguyenvanbien.rar.compress_uncompress.IInfoExecute;
import com.nguyenvanbien.rar.interact.ManagerFileInStorage;
import com.nguyenvanbien.rar.ui.MyAplication;
import com.nguyenvanbien.rar.ui.base.interfaces.Action;
import com.nguyenvanbien.rar.ui.base.presenters.BasePresenter;
import com.nguyenvanbien.rar.utils.LogData;

import java.io.File;
import java.util.List;


public class CompressPresenter extends BasePresenter implements IInfoExecute{
    private CompressUncompressMng mComUncomMng;
    private IUpdateInfor mIUpdateInfor;
    private String mProcessName;
    private boolean mIsExecuting;
    private ManagerFileInStorage mMngFile;
    private Context mContext;

    public CompressPresenter(Context context) {
        super();
        mComUncomMng = new CompressUncompressMng();
        mProcessName="";
        mIsExecuting = false;
        mMngFile = new ManagerFileInStorage();
        mContext = context;
    }

    public String getParentDir(){
        if(mComUncomMng.getParentDir()==null){
            return File.separator;
        }

        return mComUncomMng.getParentDir();
    }

    @Override
    public String getPathIsExecuting() {
        return mComUncomMng.getPathIsExecuting();
    }

    @Override
    public float getPercentExecute() {
        return mComUncomMng.getPercentExecute();
    }

    public String getProcessName() {
        return mProcessName;
    }

    public void setIUpdateInfor(IUpdateInfor iUpdateInfor) {
        this.mIUpdateInfor = iUpdateInfor;
    }

    /**********************************************
     * compress file
     * @param type type of file
     * @param name file name
     * @param listFileIsClicked list file compress
     *********************************************/
    public void compress(String type, String name, List<String> listFileIsClicked) {
        mProcessName = "Đang nén file";

        Action<Boolean> onNext = new Action<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if(!aBoolean){
                    LogData.showDialogMessage(mContext, "Error", "Nén thất bại");
                }else {
                    //display watch-file buttom
                    mIUpdateInfor.setVisibleWatchButton(true);
                    mIUpdateInfor.showMessage("Nén file thành công");
                }
            }
        };

        Action<Throwable> onError = new Action<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                LogData.showDialogMessage(mContext, "Error", "Nén thất bại");
            }
        };

        switch (type){
            case "zip":
                interact(mComUncomMng.compressZip(name, listFileIsClicked.toArray(new String[]{}))
                        , onNext, onError);
                break;

            case "rar":
                interact(mComUncomMng.compressRar(name, listFileIsClicked.toArray(new String[]{})),
                        onNext, onError);
                break;

            default:
                break;
        }
    }

//    public void uncompress(final List<String> listFileIsClicked){
//        mProcessName = "Đang giải nén";
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for(String path : listFileIsClicked){
//                    mIsExecuting = true;
//                    uncompress(path);
//
//                    //stop for
//                    while (mIsExecuting && !mIsDestroy){
//                        SystemClock.sleep(500);
//                    }
//
//                    if(mIsDestroy){
//                        break;
//                    }
//                }
//            }
//        }).start();
//    }

    public void uncompress(String path){
        mProcessName = "Đang giải nén file";

        Action<Boolean> onNext = new Action<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if(!aBoolean){
                    LogData.showDialogMessage(mContext, "Error", "Giải nén thất bại");
                }else {
                    //display watch-file buttom
                    mIUpdateInfor.setVisibleWatchButton(true);
                    mIUpdateInfor.showMessage("Nén file thành công");
                }

                mIsExecuting = false;
            }
        };

        Action<Throwable> onError = new Action<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                LogData.showDialogMessage(mContext, "Error", "Giải nén thất bại");
                mIsExecuting = false;
            }
        };

        if(path.endsWith(".zip")){
            interact(mComUncomMng.uncompressZip(path), onNext, onError);
            return;
        }

        if(path.endsWith(".rar")){
            interact(mComUncomMng.uncompressRar(path), onNext, onError);
            return;
        }

        mIsExecuting = false;

    }

    /******************
     * delete list file
     * @param mListFileIsClicked
     ******************/
    public void deleteFiles(final List<String> mListFileIsClicked) {
        final String[] path = mListFileIsClicked.toArray(new String[]{});

        new AsyncTask<Void, String, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                for(String p : path){
                    mMngFile.deleteFile(p);
                    publishProgress("Đang xóa: "+p);
                }

                publishProgress("Xóa thành công!");

                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
                Toast toast = Toast.makeText(MyAplication.APP_CONTEXT, "Xóa file", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.setText(values[0]);
                toast.show();
            }
        }.execute();

    }

    public String detailFile(String path) {
        return mMngFile.detailFile(path);
    }
}
