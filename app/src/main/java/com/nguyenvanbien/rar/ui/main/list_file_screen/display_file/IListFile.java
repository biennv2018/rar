package com.nguyenvanbien.rar.ui.main.list_file_screen.display_file;

import com.nguyenvanbien.rar.model.ItemInforFile;

interface IListFile {
    int getCount();

    ItemInforFile getItem(int position);
}
