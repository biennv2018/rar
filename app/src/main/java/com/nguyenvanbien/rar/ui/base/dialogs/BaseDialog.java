package com.nguyenvanbien.rar.ui.base.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.Window;
import android.widget.Toast;

import com.nguyenvanbien.rar.ui.base.interfaces.IBaseMethod;

public abstract class BaseDialog extends Dialog implements IBaseMethod{
    protected boolean mIsDestroy;

    public BaseDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        mIsDestroy = false;

        setContentView(setContentView());
        findViewByIds();
        inits();
        initsComponents();
        setEvents();
    }

    @Override
    public void showMessage(String content) {
        Toast toast = Toast.makeText(getContext(), content, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER|Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    @Override
    public void onDestroy() {
        mIsDestroy = true;
        cancel();
    }
}
