package com.nguyenvanbien.rar.ui.base.animations;

import com.nguyenvanbien.rar.R;

public enum TranslateAnim {
    TRANSLATE_SCREEN(R.anim.enter_to_right, R.anim.exit_to_right, R.anim.enter_to_left, R.anim.exit_to_left)
    ;

    private final int enterToRight;
    private final int exitToRight;
    private final int enterToLeft;
    private final int exitToLeft;

    TranslateAnim(int enterToRight, int exitToRight, int enterToLeft, int exitToLeft) {
        this.enterToRight = enterToRight;
        this.exitToRight = exitToRight;
        this.enterToLeft = enterToLeft;
        this.exitToLeft = exitToLeft;
    }

    public int getEnterToRight() {
        return enterToRight;
    }

    public int getExitToRight() {
        return exitToRight;
    }

    public int getEnterToLeft() {
        return enterToLeft;
    }

    public int getExitToLeft() {
        return exitToLeft;
    }
}
