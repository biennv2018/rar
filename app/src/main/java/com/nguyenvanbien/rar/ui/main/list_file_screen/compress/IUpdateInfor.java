package com.nguyenvanbien.rar.ui.main.list_file_screen.compress;

public interface IUpdateInfor {

    void setVisibleWatchButton(boolean bool);

    void updateProgress(int per);

    void updatePath(String path);

    void showMessage(String content);
}
