package com.nguyenvanbien.rar.ui.main.flash_screen;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import com.nguyenvanbien.rar.R;
import com.nguyenvanbien.rar.ui.MainActivity;
import com.nguyenvanbien.rar.ui.base.activities.BaseActivity;

/********************************************************************************************
 * man hinh yeu cau cap quyen cho ung dung
 *******************************************************************************************/
public class FlashActivity extends BaseActivity {
    private boolean mIsCheckingPermistion;

    @Override
    public int setContentView() {
        return R.layout.flash_screen;
    }

    @Override
    public void findViewByIds() {

    }

    @Override
    public void inits() {
        mIsCheckingPermistion = true;

        //yeu cau cap quyen
        RequestPermisstion permisstion = new RequestPermisstion(this);

        //start activity neu quyen duoc cap roi
        if(!permisstion.isRequestingPermisstion()){
            startMainActivity();
        }
    }

    @Override
    public void initsComponents() {

    }

    @Override
    public void setEvents() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int result : grantResults) {

            if (result == PackageManager.PERMISSION_DENIED) {
                mIsCheckingPermistion = false;
                showMessage("Vui lòng cấp quyền cho ứng dụng!\nSetting->Application->"+
                        getResources().getString(R.string.app_name)+"->Permisstion");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        System.exit(0);
                    }
                }, 3000);

                return;
            }
        }
        //start activity chinh meu dang kiem tra quyen
        if(mIsCheckingPermistion) {
            mIsCheckingPermistion = false;
            startMainActivity();
        }
    }

    /**
     * start main achtivy
     */
    private void startMainActivity(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(getBaseContext(), MainActivity.class);
                startActivity(intent);

                finish();
            }
        },1000);

    }
}
