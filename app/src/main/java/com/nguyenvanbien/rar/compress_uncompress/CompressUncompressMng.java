package com.nguyenvanbien.rar.compress_uncompress;

import com.nguyenvanbien.rar.utils.LogData;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class CompressUncompressMng implements IInfoExecute{
    private IInfoExecute entry;
    //directory containt file after compress, uncompress
    private String mParentDir;

    public CompressUncompressMng() {
    }

    public String getParentDir() {
        return mParentDir;
    }

    @Override
    public String getPathIsExecuting() {
        if(entry==null){
            return "...";
        }
        return entry.getPathIsExecuting();
    }

    @Override
    public float getPercentExecute() {
        if(entry==null){
            return 0;
        }

        return entry.getPercentExecute();
    }

    /**
     * uncompress zip file
     * @param path
     * @return result uncompress
     */
    public Observable<Boolean> uncompressZip(final String path){

        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
                ZipFile zipFile = new ZipFile();
                entry = zipFile;
                boolean result = zipFile.uncompressZip(path);

                mParentDir=zipFile.getParentDir();
                e.onNext(result);
                //e.onComplete();
            }
        });
    }

    /**
     * compress zip file
     * @param nameFile file name after compress
     * @param pathFile array file path compress
     * @return result
     */
    public Observable<Boolean> compressZip(final String nameFile, final String[] pathFile){
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
                ZipFile zipFile = new ZipFile();
                entry = zipFile;
                boolean result = zipFile.compressZip(nameFile, pathFile);

                mParentDir = zipFile.getParentDir();
                e.onNext(result);
            }
        });
    }

    /**
     * uncompress rar file
     * @param path
     * @return result
     */
    public  Observable<Boolean> uncompressRar(final String path){
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> e) throws Exception {
                RarFile rarFile = new RarFile();
                entry = rarFile;
                boolean result = rarFile.uncompressRar(path);

                mParentDir = rarFile.getParentDir();
                e.onNext(result);
            }
        });
    }

    public Observable<Boolean> compressRar(String name, String[] pathFile) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(ObservableEmitter<Boolean> e) throws Exception {

                //mParentDir = rarFile.getParentDir();
                e.onNext(false);
            }
        });
    }
}
