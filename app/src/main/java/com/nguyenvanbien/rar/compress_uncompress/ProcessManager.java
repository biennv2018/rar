package com.nguyenvanbien.rar.compress_uncompress;

import com.nguyenvanbien.rar.utils.LogData;

/****************************************************
 * Quan lý tiến trình thực thi giải nén, đóng nén file
 ****************************************************/
class ProcessManager {
    private static final String TAG = ProcessManager.class.getName();
    private String mPathIsExecuting;
    private long mTotalSize, mInstantSize;

    ProcessManager() {
        setDefaultValue();
    }

    void setDefaultValue(){
        mPathIsExecuting = null;
        mTotalSize = 0;
        mInstantSize = 1;
    }


    /**
     * get path of file is execututing
     * @return path
     */
    String getPathFileIsExecuting(){
        if(mPathIsExecuting !=null) {
            return mPathIsExecuting;
        }else {
            return "...";
        }
    }

    /**
     * get percent
     * @return
     */
    float getPercentExecuting(){
        if(mTotalSize!=0) {
            return (mInstantSize * 100 / mTotalSize);
        }else {
            return 0;
        }
    }

    public long getTotalSize() {
        return mTotalSize;
    }

    public long getInstantSize() {
        return mInstantSize;
    }

    void setPathIsExecuting(String pathIsExecuting){
        mPathIsExecuting = pathIsExecuting;
    }

    void setTotalSize(long total){
        mTotalSize = total;
    }

    void addSize(long size){
        mInstantSize = mInstantSize + size;
    }
}
