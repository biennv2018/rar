package com.nguyenvanbien.rar.compress_uncompress;

import com.nguyenvanbien.rar.utils.LogData;

import java.io.File;
import java.io.IOException;

/*********************************************************************
 * Nhung method co ban cua file
 *********************************************************************/

class BaseMethodInFile {

    BaseMethodInFile() {
    }

    /**
     * get directory save file extract
     * @param pathFile
     * @return
     */
    String getPathUncompressDir(String pathFile){
        return pathFile.substring(0, pathFile.lastIndexOf("."));
    }

    /**
     * tao thu muc root chua cac file con
     * tao file hoac thu muc moi nam trong thu muc root
     * @param path
     * @return
     */
    boolean createNewFile(String path){
        return createRootDir(path)&& createSubFile(path);
    }

    /**
     * create root directory contain all sub file
     * @param path path of sub file
     * @return result create
     */
    private boolean createRootDir(String path){
        // xac dinh path cua thu muc root
        String pathRoot = path.substring(0, path.lastIndexOf(File.separator));

        File rootFile = new File(pathRoot);

        if(rootFile == null){
            return false;
        }

        if(!rootFile.exists()){
            rootFile.mkdirs();
        }

        return  true;
    }

    /**
     * create sub file
     * @param path path of sub file
     * @return
     */
    private boolean createSubFile(String path){
        File file = new File(path);

        if(file==null){
            return false;
        }

        if(!file.exists()) {
            if (path.indexOf(".") > -1) {
                //la file
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    LogData.printStackTrace(e);
                    return false;
                }
            } else {
                file.mkdirs();
            }
        }

        return true;
    }

    /**
     * format path
     * @param path
     * @return path after format
     */
    String formatPath(String path){
        if(path.charAt(0)=='\\' || path.charAt(0)=='/'){
            path = path.substring(1);
        }

        path = path.replace("\\", File.separator);
        path = path.replace("/", File.separator);

        return path;
    }
}
