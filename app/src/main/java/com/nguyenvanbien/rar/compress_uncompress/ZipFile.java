package com.nguyenvanbien.rar.compress_uncompress;

import com.nguyenvanbien.rar.interact.ManagerFileInStorage;
import com.nguyenvanbien.rar.utils.LogData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/************************************************************************************************
 * manager zip file: compress file, uncompress file
 ***********************************************************************************************/
class ZipFile implements IInfoExecute{
    private static final String TAG = ZipFile.class.getSimpleName();
    private ProcessManager mMngProcess;
    private BaseMethodInFile mBaseMethod;
    private String mParentDir;

    public ZipFile() {
        super();

        mMngProcess = new ProcessManager();
        mBaseMethod = new BaseMethodInFile();
        mParentDir= File.separator;
    }

    public String getParentDir() {
        return mParentDir;
    }

    @Override
    public String getPathIsExecuting() {
        return mMngProcess.getPathFileIsExecuting();
    }

    @Override
    public float getPercentExecute() {
        return mMngProcess.getPercentExecuting();
    }

    /**
     * uncompress file zip
     * @param pathFile path of file
     * @return result extract
     */
    protected boolean uncompressZip(String pathFile){
        LogData.log(TAG, "uncompressZip", "---------------------\nGiải nén file: "+pathFile);

        mMngProcess.setDefaultValue();
        //tao thuc muc luu file giai nen
        String pathDir = mBaseMethod.getPathUncompressDir(pathFile);
        mParentDir = pathDir;
        mBaseMethod.createNewFile(pathDir);

        ZipInputStream zipInputStream=null;
        try {
            //khoi tao gia tri
            File fileZip = new File(pathFile);
            mMngProcess.setTotalSize(getCountEntry(pathFile));
            LogData.log(TAG, "uncompressZip", "TotalSize="+ mMngProcess.getTotalSize());

            zipInputStream = new ZipInputStream(new FileInputStream(fileZip));
            ZipEntry zipEntry;

            //giai nen file
            while ((zipEntry=zipInputStream.getNextEntry())!=null){

                //update duong dan dang xu ly
                mMngProcess.setPathIsExecuting(pathDir+File.separator
                        +mBaseMethod.formatPath(zipEntry.getName()));
                LogData.log(TAG, "uncompressZip", "PathFileIsExecuting: "
                        +mMngProcess.getPathFileIsExecuting());
                //update kich thuoc file dang xu ly
                mMngProcess.addSize(1);
                LogData.log(TAG, "uncompressZip", "InstantSize="+ mMngProcess.getInstantSize());

                //copy file hoac thu muc
                mBaseMethod.createNewFile(mMngProcess.getPathFileIsExecuting());
                if(!zipEntry.isDirectory()){
                    //tao file du lieu
                    FileOutputStream output = new FileOutputStream(mMngProcess.getPathFileIsExecuting());
                    byte buffer[] = new byte[1024];
                    int length;

                    while ((length=zipInputStream.read(buffer))>-1){
                        output.write(buffer, 0, length);
                    }

                    output.close();
                }

                zipInputStream.closeEntry();
            }

            zipInputStream.close();
        } catch (FileNotFoundException e) {
            LogData.printStackTrace(e);
            return false;
        } catch (IOException e) {
            LogData.printStackTrace(e);
            return false;
        }

        return true;
    }

    /**
     * count quantity entry in zip file
     * @param pathFile
     * @return
     */
    private int getCountEntry(String pathFile) {
        int count = 0;

        try {
            ZipInputStream zipInput = new ZipInputStream(new FileInputStream(pathFile));
            while (zipInput.getNextEntry()!=null){
                count = count + 1;
            }
        } catch (FileNotFoundException e) {
            return 0;
        } catch (IOException e) {
            return 0;
        }
        return count;
    }

    /**
     * compress file zip
     * @param nameFile name of file after compress
     * @param pathFile array file compress
     * @return result compress
     */
    protected boolean compressZip(String nameFile, String[] pathFile){
        LogData.log(TAG, "compressZip", "-------------------------\nĐang nén file ...");
        mMngProcess.setDefaultValue();
        mMngProcess.setTotalSize(pathFile.length);

        try {
            String pathZip = pathFile[0].substring(0, pathFile[0].lastIndexOf(File.separator));
            //mParentDir = pathZip+File.separator+nameFile;
            mParentDir = pathZip;
            ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(pathZip+File.separator+nameFile));

            byte buffer[] = new byte[1024];
            int le=0;
            List<String> listFileInDir;

            for(String path : pathFile){
                //update infor
                mMngProcess.setPathIsExecuting(path);
                LogData.log(TAG, "compressZip", "PathFileIsExecuting: "
                        +mMngProcess.getPathFileIsExecuting());
                mMngProcess.addSize(1);
                LogData.log(TAG, "uncompressZip", "InstantSize="+ mMngProcess.getInstantSize());

                //compress file
                File fileInput = new File(path);
                if (!fileInput.exists()){
                    continue;
                }

                //get list file in directory
                listFileInDir = new ArrayList<>();
                if(fileInput.isDirectory()){
                    listFileInDir.addAll(new ManagerFileInStorage().getAllFile(path));
                }else {
                    listFileInDir.add(path);
                }

                //copy file
                for(String tempPath : listFileInDir) {
                    fileInput = new File(tempPath);
                    FileInputStream input = new FileInputStream(fileInput);

                    //tempPath = pathZip/xxx
                    //xxx is a path contain parent directory and file name
                    ZipEntry zipEntry = new ZipEntry(tempPath.substring(pathZip.length()+1));
                    zipOutput.putNextEntry(zipEntry);

                    while ((le = input.read(buffer)) > -1) {
                        zipOutput.write(buffer, 0, le);
                    }

                    input.close();
                    zipOutput.closeEntry();
                }
            }

            zipOutput.close();
        } catch (FileNotFoundException e) {
            LogData.printStackTrace(e);
            return false;
        } catch (IOException e) {
            LogData.printStackTrace(e);
            return false;
        }

        return true;
    }
}
