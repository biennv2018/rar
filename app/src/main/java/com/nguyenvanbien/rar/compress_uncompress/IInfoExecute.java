package com.nguyenvanbien.rar.compress_uncompress;

/**
 * chua cac method tra ve thong tin nen hoac giai nen file
 */
public interface IInfoExecute {
    String getPathIsExecuting();
    float getPercentExecute();
}
