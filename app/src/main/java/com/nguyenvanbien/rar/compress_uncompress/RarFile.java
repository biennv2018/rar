package com.nguyenvanbien.rar.compress_uncompress;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.rarfile.FileHeader;
import com.nguyenvanbien.rar.utils.LogData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class RarFile implements IInfoExecute{
    private static final String TAG = RarFile.class.getSimpleName();
    private ProcessManager mProcessMng;
    private BaseMethodInFile mBaseMethod;
    private String mParentDir;

    public RarFile() {
        mProcessMng = new ProcessManager();
        mBaseMethod = new BaseMethodInFile();
        mParentDir = File.separator;
    }

    public String getParentDir() {
        return mParentDir;
    }

    @Override
    public String getPathIsExecuting() {
        return mProcessMng.getPathFileIsExecuting();
    }

    @Override
    public float getPercentExecute() {
        return mProcessMng.getPercentExecuting();
    }

    /**
     * uncompress rar file
     * @param pathFile path of rar file
     * @return result uncompress
     */
    protected boolean uncompressRar(String pathFile){
        LogData.log(TAG, "uncompressRar", "-----------------------------\nGiai nen: "+pathFile);

        mProcessMng.setDefaultValue();
        //create directory uncompress rar file
        String pathDir = mBaseMethod.getPathUncompressDir(pathFile);
        mParentDir = pathDir;
        mBaseMethod.createNewFile(pathDir);

        try {
            File fileRar = new File(pathFile);
            Archive rarInput = new Archive(fileRar);
            mProcessMng.setTotalSize(rarInput.getFileHeaders().size());
            LogData.log(TAG, "uncompressRar", "Total: "+mProcessMng.getTotalSize());
            FileHeader fileHeader;

            while ((fileHeader=rarInput.nextFileHeader())!=null){
                //update infor uncompress
                mProcessMng.setPathIsExecuting(pathDir+File.separator
                        +mBaseMethod.formatPath(fileHeader.getFileNameString()));
                LogData.log(TAG, "uncompressRar", "FilePathIsExecuting: "
                        +mProcessMng.getPathFileIsExecuting());
                mProcessMng.addSize(1);
                LogData.log(TAG, "uncompressRar", "InstantSize="+mProcessMng.getInstantSize());

                //copy file or directory
                mBaseMethod.createNewFile(mProcessMng.getPathFileIsExecuting());
                if(!fileHeader.isDirectory()){
                    FileOutputStream out = new FileOutputStream(mProcessMng.getPathFileIsExecuting());
                    rarInput.extractFile(fileHeader, out);
                    out.close();
                }
            }

            rarInput.close();

        } catch (RarException e) {
            LogData.printStackTrace(e);
            return false;
        } catch (IOException e) {
            LogData.printStackTrace(e);
            return false;
        }

        return true;
    }

    /**
     * compress rar file
     * @param nameFile file name compress
     * @param pathFile array path of compress file
     * @return result compress
     */
    protected boolean compressRar(String nameFile, String[] pathFile){

        return false;
    }

}
