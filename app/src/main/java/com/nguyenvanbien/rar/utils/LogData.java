package com.nguyenvanbien.rar.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import static com.nguyenvanbien.rar.utils.constant.Constants.RELEASE_APP;

public class LogData {

    public static void log(String tag, String nameMothod, String data){
        if(!RELEASE_APP) {
            Log.d(tag + "/" + nameMothod + "():", data+"");
        }
    }

    public static <E extends Exception> void printStackTrace( E e){
        if(!RELEASE_APP) {
            e.printStackTrace();
        }
    }

    public static void showDialogMessage(Context context, String title, String content){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(content);
        alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}
