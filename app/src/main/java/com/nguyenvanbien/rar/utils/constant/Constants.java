package com.nguyenvanbien.rar.utils.constant;

import com.nguyenvanbien.rar.R;

public interface Constants {
    String PACKAGE_NAME = "com.nguyenvanbien.rar";
    boolean RELEASE_APP = false;
    int COLOR_VIEW_IS_CLICKED = R.color.colorAccent;
    int DEFAULT_COLOR_VIEW = R.color.grey_50;
}
