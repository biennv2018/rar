package com.nguyenvanbien.rar.interact;

import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.nguyenvanbien.rar.model.ItemInforFile;
import com.nguyenvanbien.rar.ui.MyAplication;
import com.nguyenvanbien.rar.utils.LogData;

import org.apache.commons.vfs2.FileSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/*******************************************************************************************
 * Manager files in storage
 * get infor file
 ******************************************************************************************/
public class ManagerFileInStorage {
    private static final String TAG = ManagerFileInStorage.class.getSimpleName();

    public ManagerFileInStorage() {
    }

    /**
     * get list file in a directory
     * @param dirPath
     * @return
     */
    public List<ItemInforFile> getFilesInDir(String dirPath){
        LogData.log(TAG, "getFileInDir", "Get infor file in directory");
        List<ItemInforFile> listFile = new ArrayList<>();

        File dir = new File(dirPath);
        if(dir.isDirectory()){
            return getFilesInDir(dir, dirPath);
        }

//        if(dir.isFile()){
//            if(dirPath.endsWith(".zip")){
//                return getFileInsZipDir(dir);
//            }
//
//            if(dirPath.endsWith(".rar")){
//                return getFilesInRarDir(dir);
//            }
//
//        }

        return listFile;
    }

    private List<ItemInforFile> getFilesInDir(File dir, String dirPath) {
        List<ItemInforFile> listFile = new ArrayList<>();
        String[] filePath = dir.list();
        if(filePath!=null&&filePath.length>0) {

            for (String path : filePath) {
                File file = new File(dirPath + File.separator + path);
                String nameF = file.getName();
                String pathF = file.getPath();
                long dateEditF = file.lastModified();

                listFile.add(new ItemInforFile(nameF, pathF, dateEditF));
            }
        }
        return listFile;
    }

    private List<ItemInforFile> getFilesInRarDir(File dir) {
        return null;
    }

    private List<ItemInforFile> getFileInsZipDir(File dir) {
//        List<ItemInforFile> listFile = new ArrayList<>();
//
//        try {
//            ZipInputStream zipInput = new ZipInputStream(new FileInputStream(dir));
//            ZipEntry zipEntry = zipInput.getNextEntry();
//
//            while (zipEntry!=null){
//                listFile.add(new ItemInforFile(zipEntry.getName(), zipEntry.))
//            }
//        } catch (FileNotFoundException e) {
//            //e.printStackTrace();
//        } catch (IOException e) {
//            //e.printStackTrace();
//        }
        return null;
    }

    /**************************
     * get all compress file
     * @return list infor file
     *************************/
    public List<ItemInforFile> getAllCompressFile(){

        return getListFile();
    }

    /**
     * get all compress file in device
     * @return list file infor
     */
    private List<ItemInforFile> getListFile() {
        List<ItemInforFile> listInforFile = new ArrayList<>();
        Cursor cursor = queryFile();

        if(cursor!=null){
            int nameIndext = cursor.getColumnIndex(MediaStore.Files.FileColumns.TITLE);
            int pathIndext = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
            int dateModifyIndext = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATE_MODIFIED);

            cursor.moveToFirst();
            LogData.log(TAG, "getListFile", "-----------------------\nAll file rar");
            String name, path;
            long date;
            while(!cursor.isAfterLast()){
                name = cursor.getString(nameIndext);
                path = cursor.getString(pathIndext);
                date = cursor.getLong(dateModifyIndext);
                LogData.log(TAG, "getListFile", path);
                listInforFile.add(new ItemInforFile(name, path, date));
                cursor.moveToNext();
            }
        }

        return listInforFile;
    }

    /**
     * query compress file
     * @return a cursor after query
     */
    private Cursor queryFile() {
        //get list file in storage divice
        Uri uri = MediaStore.Files.getContentUri("external");
        String filePathColumn = MediaStore.Files.FileColumns.DATA;
        String dateModifyColumn = MediaStore.Files.FileColumns.DATE_MODIFIED;

        String[] fileType = new String[]{
                "'%.rar'",
                "'%.zip'",
        };
        String whereClause = filePathColumn+" like "+fileType[0]+" ";
        for(int i=1; i<fileType.length; i++){
            whereClause = whereClause + " or "+filePathColumn+" like "+fileType[i]+" ";
        }

        String orderByClause = dateModifyColumn+" desc";

        return MyAplication.APP_CONTEXT.getContentResolver()
                .query(uri, null, whereClause , null, orderByClause);
    }


    /*************************************
     * get all file in a directory
     * @param pathRoot path of directory
     * @return a list contain path of file
     *************************************/
    public List<String> getAllFile(String pathRoot){
        return getListFiles(pathRoot, ".*");
    }


    /**************************************************************
     * get list files have tag equal {@code fileTag} in a directory
     * @param pathRoot path of directory
     * @param fileTag tag of search file:
     *                all file is .*
     *                type file is .type
     * @return a list contain path of file
     **************************************************************/
    public List<String> getListFiles(String pathRoot, String fileTag){
        List<String> listFile = new ArrayList<>();
        File root = new File(pathRoot);
        if (!root.exists()) {
            return listFile;
        }

        for(File file : root.listFiles()) {
            if(file.isDirectory()){
                listFile.addAll(getListFiles(file.getPath(), fileTag));
                continue;
            }

            if(file.isFile()){
                if(!fileTag.equals(".*")){
                    if(file.getName().endsWith(fileTag)){
                        listFile.add(file.getPath());
                    }
                }else {
                    listFile.add(file.getPath());
                }
            }
        }

        return listFile;
    }

    public void deleteFile(String path){
        File file = new File(path);

        if(!file.exists()){
            return;
        }

        if(file.isFile()){
            deleteFile(file);
            return;
        }

        if(file.isDirectory()){
            String[] paths = file.list();

            for(String p : paths){
                deleteFile(path+File.separator+p);
            }

            file.delete();
        }
    }

    private void deleteFile(File file){
        if(file.exists()){
            file.delete();
        }
    }

    public String detailFile(String path) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        String detail = "";

        File file = new File(path);
        detail = "Tên: "+file.getName()
                +"\nĐường dẫn: "+file.getPath()
                +"\nKích thước: "+file.length()+" byte"
                +"\nNgày chỉnh sửa: "+sdf.format(file.lastModified());
        return detail;
    }

    /***************************
     * get free space of storage
     * @return size (GB)
     ***************************/
    public double getFreeSpaceStorage(){
        File file = Environment.getExternalStorageDirectory();
        return (file.getFreeSpace()/Math.pow(1024, 3));
    }

    public double getTotalSpaceStorage(){
        File file = Environment.getExternalStorageDirectory();
        return (file.getTotalSpace()/Math.pow(1024, 3));
    }
}
